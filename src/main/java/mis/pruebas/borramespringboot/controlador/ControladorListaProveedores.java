package mis.pruebas.borramespringboot.controlador;

import mis.pruebas.borramespringboot.modelo.Producto;
import mis.pruebas.borramespringboot.modelo.Proveedor;
import mis.pruebas.borramespringboot.servicio.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.v1}/proveedores")
public class ControladorListaProveedores {

    @Autowired
    ServicioProveedor servicioProveedor;

    @PostMapping
    public void agregarProveedor(@RequestBody Proveedor p){
        this.servicioProveedor.agregar(p);
    }

    @GetMapping
    public List<Proveedor> obtenerProovedores(){
        return this.servicioProveedor.obtenerProveedores();
    }
}
