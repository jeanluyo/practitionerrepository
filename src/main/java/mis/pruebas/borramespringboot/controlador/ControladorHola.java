package mis.pruebas.borramespringboot.controlador;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/saludo")
public class ControladorHola {

    private String saludo = "Hola";

    //@RequestMapping(method = RequestMethod.GET, path = "/hola")
    @GetMapping
    public String saludar(@RequestParam(required = false, name = "persona") String nombre){
        if(nombre == null || nombre.trim().isEmpty())
            return String.format("%s mundo!", this.saludo);

        return String.format("%s %s!", this.saludo, nombre);
    }

    //@RequestParam - Via parametros ?saludo=
    //@RequestBody  - Via Body
    @PutMapping
    public ResponseEntity guardarNuevoSaludo(@RequestParam(name = "saludo") String saludoNuevo) {
        if (saludoNuevo != null && !saludoNuevo.trim().isEmpty()){
            this.saludo = saludoNuevo.trim();
            return ResponseEntity.ok("Se guardó el saludo: "+ this.saludo);
        }
        else{
            return ResponseEntity
                    .status(403)
                    .body("El saludo no puede estar vacio.");
            //throw new ResponseStatusException(HttpStatus.FORBIDDEN, "El saludo no existe");
        }
    }
}

/*
* / hola -> ""
*
*
 */
