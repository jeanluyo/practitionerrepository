package mis.pruebas.borramespringboot.controlador;

import mis.pruebas.borramespringboot.modelo.Producto;
import mis.pruebas.borramespringboot.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${api.v1}/productos/{idProducto}")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping
    public ResponseEntity<Producto> obtenerProducto(@PathVariable long idProducto){
        try {
            return ResponseEntity.ok(buscarProductoPorId(idProducto));
        } catch(ResponseStatusException x) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping
    public void reemplazarProducto(@PathVariable(name = "idProducto") long id,
                                   @RequestBody Producto p){
        buscarProductoPorId(id);
        p.id = id;
        this.servicioProducto.reemplazarProducto(id,p);
    }

    @DeleteMapping
    public void borrarProducto(@PathVariable(name = "idProducto") long id ){
        this.servicioProducto.eliminarProducto(id);
    }

    private Producto buscarProductoPorId(long idProducto) {
        final Producto p = this.servicioProducto.obtenerProducto(idProducto);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
