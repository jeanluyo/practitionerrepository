package mis.pruebas.borramespringboot.controlador;

import mis.pruebas.borramespringboot.modelo.Proveedor;
import mis.pruebas.borramespringboot.servicio.ServicioProveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("${api.v1}/proveedores/{idProveedor}")
public class ControladorProveedor {

    @Autowired
    ServicioProveedor servicioProveedor;

    @GetMapping
    public ResponseEntity<Proveedor> obtenerProveedor(@PathVariable long idProveedor){
        try {
            return ResponseEntity.ok(buscarProveedorPorId(idProveedor));
        } catch(ResponseStatusException x) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping
    public void reemplazarProveedor(@PathVariable(name = "idProveedor") long id,
                                   @RequestBody Proveedor p){
        buscarProveedorPorId(id);
        p.id = id;
        this.servicioProveedor.reemplazarProveedor(id,p);
    }

    @DeleteMapping
    public void borrarProveedor(@PathVariable(name = "idProveedor") long id ){
        this.servicioProveedor.eliminarProveedor(id);
    }

    private Proveedor buscarProveedorPorId(long idProveedor) {
        final Proveedor p = this.servicioProveedor.obtenerProveedor(idProveedor);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
