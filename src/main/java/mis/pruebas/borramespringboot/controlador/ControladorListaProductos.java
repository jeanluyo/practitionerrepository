package mis.pruebas.borramespringboot.controlador;

import mis.pruebas.borramespringboot.modelo.Producto;
import mis.pruebas.borramespringboot.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.v1}/productos")
public class ControladorListaProductos {

    @Autowired
    ServicioProducto servicioProducto;

    @PostMapping
    public void agregarProducto(@RequestBody Producto p){
        this.servicioProducto.agregar(p);
    }

    @GetMapping
    public List<Producto> obtenerProductos(){
        return this.servicioProducto.obtenerProductos();
    }
}
