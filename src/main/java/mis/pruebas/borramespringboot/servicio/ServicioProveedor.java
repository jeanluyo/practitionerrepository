package mis.pruebas.borramespringboot.servicio;

import mis.pruebas.borramespringboot.modelo.Producto;
import mis.pruebas.borramespringboot.modelo.Proveedor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioProveedor {
    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Proveedor> proveedores = new ConcurrentHashMap<Long, Proveedor>();

    public long agregar(Proveedor p){
        p.id = this.secuenciador.incrementAndGet();
        this.proveedores.put(p.id,p);
        return p.id;
    }

    public List<Proveedor> obtenerProveedores(){
        return this.proveedores
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }

    public Proveedor obtenerProveedor(long id){
        final Proveedor p = this.proveedores.get(id);
        return p;
    }

    public void reemplazarProveedor(long id, Proveedor p){
        p.id = id;
        this.proveedores.put(id,p);
    }

    public void eliminarProveedor(long id){
        this.proveedores.remove(id);
    }

}
