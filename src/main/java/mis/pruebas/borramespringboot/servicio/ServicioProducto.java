package mis.pruebas.borramespringboot.servicio;

import mis.pruebas.borramespringboot.modelo.Producto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ServicioProducto {

    private final AtomicLong secuenciador = new AtomicLong(0);
    private final ConcurrentHashMap<Long, Producto> productos = new ConcurrentHashMap<Long, Producto>();

    public long agregar(Producto p){
        p.id = this.secuenciador.incrementAndGet();
        this.productos.put(p.id,p);
        return p.id;
    }

    public Producto obtenerProducto(long id){
        final Producto p = this.productos.get(id);
        return p;
    }

    public List<Producto> obtenerProductos(){
        return this.productos
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }

    public void reemplazarProducto(long id, Producto p){
        p.id = id;
        this.productos.put(id,p);
    }

    public void eliminarProducto(long id){
        this.productos.remove(id);
    }
}
