package mis.pruebas.borramespringboot.modelo;

public class Producto {
    public long id;
    public String nombre;
    public double precio;
    public double cantidad;
}
