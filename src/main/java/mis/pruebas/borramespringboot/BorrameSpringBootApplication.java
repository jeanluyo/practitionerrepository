package mis.pruebas.borramespringboot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class BorrameSpringBootApplication {
	public static void main(String[] args) {
		SpringApplication.run(BorrameSpringBootApplication.class, args);
	}

	@Bean
	public CommandLineRunner lineaComando(ApplicationContext app){
		return args ->{
			final String[] beans =app.getBeanDefinitionNames();
			System.out.println("Nro Beans:"+ app.getBeanDefinitionCount());
			Arrays.sort(beans);
			for(String b: beans){
				System.out.println("Bean: "+ b);
			}
		};
	}
}
